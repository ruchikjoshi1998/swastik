<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/events','LinkController@eventForm');
Route::get('/gallery','LinkController@galleryForm');
Route::get('/admin_login','LinkController@adminLoginForm');
Route::post('/logincheck','AdminController@login');

Route::group(['middleware' => 'LoginCheck'],function(){

	Route::get('/dashboard','LinkController@dashboard');
	Route::get('/logout', 'AdminController@logoutAdmin');
	Route::get('/add_faculty','LinkController@addFaculty');
	Route::post('/addFacultytoDB','AdminController@addFaculty');
	Route::get('/view_faculty','LinkController@viewFaculty');	
	Route::get('/delete_faculty/{id}','AdminController@deleteFaculty');
	
});


Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
