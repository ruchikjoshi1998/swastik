<!DOCTYPE html>
<!--[if IE 8]> <html class="ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Swastik Study Center</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <link rel="stylesheet" media="all" href="user/css/style.css">
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>

    <header id="header">
        <div class="container">
            <a href="/" id="logo" title="Swastik Study Center">Swastik</a>
            <div class="menu-trigger"></div>
            <nav id="menu">
                <ul>
                    <li><a href="/admin_login">Login</a></li>
                    <li><a href="/events">Events</a></li>
                </ul>
                <ul>
                    <li><a href="/gallery">Gallery</a></li>
                    <li><a href="/contact">Contact</a></li>
                </ul>
            </nav>
            <!-- / navigation -->

        </div>
        <!-- / container -->
    
    </header>
    <!-- / header -->

    <div class="divider"></div>
    