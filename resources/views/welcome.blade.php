@include('layout.header')
    <div class="slider">
        <ul class="bxslider">
            <li>
                <div class="container">
                    <div class="info">
                        <h2>Welcome to <br><span>Swastik Study Center</span></h2>
                        <a href="#">Check out our new programs</a>
                    </div>
                </div>
                <!-- / content -->
            </li>
            <li>
                <div class="container">
                    <div class="info">
                        <h2>Welcome to <br><span>Swastik Study Center</span></h2>
                        <a href="#">Check out our new programs</a>
                    </div>
                </div>
                <!-- / content -->
            </li>
            <li>
                <div class="container">
                    <div class="info">
                        <h2>Welcome to <br><span>Swastik Study Center</span></h2>
                        <a href="#">Check out our new programs</a>
                    </div>
                </div>
                <!-- / content -->
            </li>
        </ul>
    
    </div>
    
    <section class="posts">
        <div class="container">
            <article>
                <div class="pic"><img width="121" src="images/2.png" alt=""></div>
                <div class="info">
                    <h3>The best learning methods</h3>
                    <p>In our organization we are offering coaching for B.com, BBA, M.Com, MBA and CA-CPT. We believe that if we can give best knowledge to our students then it's more precious thing for us.</p>
                </div>
            </article>
            <article>
                <div class="pic"><img width="121" src="images/3.png" alt=""></div>
                <div class="info">
                    <h3>Awesome results of our students</h3>
                    <p>We believe that we are on our duty to give great knowledge and best education to our society. We are constantly focusing on innovation in our teaching system. We believe in the simple not the complex.</p>
                </div>
            </article>
        </div>
        <!-- / container -->
    </section>

    <section class="news">
        <div class="container">
            <h2>Latest news</h2>
            <article>
                <div class="pic"><img src="images/1.png" alt=""></div>
                <div class="info">
                    <h4>Omnis iste natus error sit voluptatem accusantium </h4>
                    <p class="date">14 APR 2014, Jason Bang</p>
                    <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores (...)</p>
                    <a class="more" href="#">Read more</a>
                </div>
            </article>
            <article>
                <div class="pic"><img src="images/1_1.png" alt=""></div>
                <div class="info">
                    <h4>Omnis iste natus error sit voluptatem accusantium </h4>
                    <p class="date">14 APR 2014, Jason Bang</p>
                    <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores (...)</p>
                    <a class="more" href="#">Read more</a>
                </div>
            </article>
            <div class="btn-holder">
                <a class="btn" href="#">See archival news</a>
            </div>
        </div>
        <!-- / container -->
    </section>

    <section class="events">
        <div class="container">
            <h2>Our Faculties</h2>
            <article>
                <center><div class="info">
                    <img src="images/fac1.jpg" alt="">
                    
                </div>
                <div class="info">
                    <h3>Nayan Gor</h3>
                    
                </div></center>
            </article>
            <article>
                <center><div class="info">
                    <img src="images/fac2.jpg" alt="">
                    
                </div>
                <div class="info">
                    <h3>Kamal Thakkar</h3>
                    
                </div></center>
            </article>
            
            <article>
                <center><div class="info">
                    <img src="images/fac3.jpg" alt="">
                    
                </div>
                <div class="info">
                    <h3>Heer Soni</h3>
                    
                </div></center>
            </article>
            
            <article>
                <center><div class="info">
                    <img src="images/fac3.jpg" alt="">
                    
                </div>
                <div class="info">
                    <h3>Nishi Thakkar</h3>
                    
                </div></center>
            </article>
            <article>
                <center><div class="info">
                    <img src="images/fac1.jpg" alt="">
                    
                </div>
                <div class="info">
                    <h3>Nayan Gor</h3>
                    
                </div></center>
            </article>
            <article>
                <center><div class="info">
                    <img src="images/fac1.jpg" alt="">
                    
                </div>
                <div class="info">
                    <h3>Nayan Gor</h3>
                    
                </div></center>
            </article>
            <div class="btn-holder">
                
            </div>
        </div>
        <!-- / container -->
    </section>
    <div class="container">
        <a href="#fancy" class="info-request">
            <span class="holder">
                <span class="title">Request information</span>
                <span class="text">Do you have some questions? Fill the form and get an answer!</span>
            </span>
            <span class="arrow"></span>
        </a>
    </div>

     @include('layout.footer')
