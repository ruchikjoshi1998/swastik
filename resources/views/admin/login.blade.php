@section('pagetitle','Login Page')
@include('admin.layout.header')
<body class="login-container">
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Unlock user -->
					{{ Form::open(array('url' => '/logincheck', 'class' => 'login-form')) }}
						<div class="panel">
							<div class="panel-body">
								

								<h6 class="content-group text-center text-semibold no-margin-top">LOGIN<small class="display-block"></small></h6>
								<div class="form-group has-feedback">
									<input type="text" name="username" class="form-control" placeholder="Your Username">
									<div class="form-control-feedback">
										<i class="icon-user-lock text-muted"></i>
									</div>
								</div>

								<div class="form-group has-feedback">
									<input type="password" name="password" class="form-control" placeholder="Your password">
									<div class="form-control-feedback">
										<i class="icon-user-lock text-muted"></i>
									</div>
								</div>

								<button type="submit" name="submit" class="btn btn-primary btn-block">LOGIN <i class="icon-arrow-right14 position-right"></i></button>
							</div>
						</div>
					{{ Form::close() }}
					<!-- /unlock user -->


					<!-- Footer -->
					<div class="footer text-muted text-center">
						&copy; 2019. <a href="#">SWASTIK</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Ruchik Joshi</a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>

<!-- Mirrored from demo.interface.club/limitless/layout_1/LTR/default/login_simple.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 18 May 2018 11:29:54 GMT -->
</html>
@include('admin.layout.footer')
