<!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- Main navigation -->
                        <div class="sidebar-category sidebar-category-visible">
                            <div class="category-content no-padding">
                                <ul class="navigation navigation-main navigation-accordion">

                                    <!-- Main -->

                                    <li class=""><a href="/dashboard"><i class="icon-home4"></i> <span>Dashboard</span></a>
                                    
                                    </li>
                                    <li>
                                        <a href="/attendence"><i class="icon-users4"></i> <span>Attendence</span></a>

                                    </li>
                                    <li>
                                        <a href="/add_papers"><i class="icon-list"></i> <span>Add Papers</span></a>

                                    </li>
                                    @if($data->name == 'admin')
                                    <li>
                                        <a href="/add_student"><i class=" icon-user"></i> <span>Add Student</span></a>

                                    </li>

                                    <li>
                                        <a href="/add_faculty"><i class=" icon-user-tie"></i> <span>Add Faculty</span></a>

                                    </li>
                                    <li>
                                        <a href="/view_faculty"><i class=" icon-user-tie"></i> <span>View Faculties</span></a>

                                    </li>
                                    @endif
                                    <!-- /layout -->

                                </ul>
                            </div>
                        </div>
                        <!-- /main navigation -->

                    </div>
                </div>
                <!-- /main sidebar -->