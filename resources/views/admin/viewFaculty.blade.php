@section('pagetitle','Faculties')
@include('admin.layout.header')

    <body>
    	@section('Name',$data->name)
        @include('admin.layout.navbar')
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                @include('admin.layout.sidebar')
                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Content area -->
                    <div class="content">

                        <!-- Basic initialization -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
         
                                <h5 class="panel-title">All Faculties</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>

                                    </ul>
                                </div>
                            </div>
						    <table class="table datatable-button-html5-basic">
                                <thead>
                                    
                                        <tr class="bg-teal-400">
                                            <th>Photo</th> 
                                            <th>Name</th>
                                            <th>Phone no.</th>
                                            <th>Joining Date</th>
                                            <th>Email</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    
                                </thead>
                                <tbody>
                                    @foreach($faculty as $faculty)
                                    <tr>
                                        <td><img height="80px" src="img/user_image/{{ $faculty->image}}">
                                        </td>
                                        <td><b><b>{{ $faculty->name }}</b></b></td>
                                        <td><b>{{ $faculty->phone }}</b></td>
                                        <td><b>{{ $faculty->joining_date }}</b></td>
                                        <td><b>{{ $faculty->email }}</b></td>
                                        <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal{{ $faculty->id }}">Edit</button></td>
                                        <td><a href="/delete_faculty/{{ $faculty->id }}" ><button type="button" class="btn btn-danger" >Delete</button></td>
                                        
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /basic initialization -->

                        

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>

    <!-- Mirrored from demo.interface.club/limitless/layout_1/LTR/default/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 18 May 2018 11:14:21 GMT -->
</html>



