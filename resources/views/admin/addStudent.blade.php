@section('pagetitle','Add Student')
@include('admin.layout.header')

    <body>
    	@section('Name',$data->name)
        @include('admin.layout.navbar')
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                @include('admin.layout.sidebar')
                <!-- Main content -->
            <div class="content-wrapper">
                <!-- Content area -->
                <div class="content">
                            <!-- Advanced legend -->
                {{ Form::open(array('url' => '/addStudenttoDB', 'class' => 'form-horizontal', 'files'=>'true')) }}
                <div class="panel panel-flat">
                    <div class="panel-body">
                    <fieldset>
                        <div class="collapse in" id="demo1">
                        <div class="form-group">
                            {{ Form::label('Name :','',array('class' => 'col-lg-3 control-label font-weight-bold')) }}
                            <div class="col-lg-6">
                            {{ Form::text('name', $value=old('name'),array('class' => 'form-control','placeholder' => 'Full Name')) }}
                            @if(count($errors) > 0)
                                @foreach($errors->get('name') as $er)
                                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                                @endforeach
                            @endif
                            </div>
                        </div>          
                       <div class="form-group">
                           {{ Form::label('Mobile Number :','',array('class' => 'col-lg-3 control-label font-weight-bold')) }}
                           <div class="col-lg-6">
                            {{ Form::number('phone', $value=old('phone') ,array('class' => 'form-control','placeholder' => 'Phone Number')) }}
                            @if(count($errors) > 0)
                                @foreach($errors->get('phone') as $er)
                                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                                @endforeach
                            @endif
                            </div>
                       </div>
                       <div class="form-group">
                           {{ Form::label('Profile Photo :','',array('class' => 'col-lg-3 control-label font-weight-bold')) }}
                           <div class="col-lg-6">
                            {{ Form::file('image')  }}
                            @if(count($errors) > 0)
                                @foreach($errors->get('image') as $er)
                                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                                @endforeach
                            @endif
                            </div>
                        </div>
                       <div class="form-group">
                            {{ Form::label('Email :','',array('class' => 'col-lg-3 control-label font-weight-bold')) }}
                            <div class="col-lg-6">
                            {{ Form::email('email', $value=old('email'),array('class' => 'form-control','placeholder' => 'Email')) }}
                            @if(count($errors) > 0)
                                @foreach($errors->get('email') as $er)
                                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                                @endforeach
                            @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('Password :','',array('class' => 'col-lg-3 control-label font-weight-bold ')) }}
                             <div class="col-lg-6">
                            {{Form::password("Password", 
                             [
                                "class" => "form-control",
                                "placeholder" => "Your Password",
                             ])
                            }}
                            @if(count($errors) > 0)
                                @foreach($errors->get('Password') as $er)
                                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                                @endforeach
                            @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('Confirm Password :','',array('class' => 'col-lg-3 control-label font-weight-bold ')) }}
                            <div class="col-lg-6">
                            {{Form::password("Password_confirmation", 
                             [
                                "class" => "form-control",
                                "placeholder" => "Confirm Password",
                             ])
                            }}
                            </div>
                        </div>
                       <div class="form-group">
                           <label class="col-lg-3 control-label">Date</label>
                           <div class="col-lg-6">
                           <div class="input-group">
                           <span class="input-group-addon"><i class=" icon-calendar"></i></span>
                           <input type="date" name="date" class="form-control pickadate" required placeholder="Try me&hellip;">
                            </div>
                            </div>
                        </div>
                        <div class="form-group">
                        <div class="text-justify">
                            <button type="submit" class="btn bg-teal-400 " name="buttn">Submit
                            <i class="icon-checkmark4 position-rifht"></i>
                            </button>
                            <button type="reset" class="btn bg-teal-400 ">
                            Clear
                            <i class="icon-eraser position-right"></i>
                            </button>
                        </div>
                        </div>
                    </div>
                    </fieldset>
                                        
                </div>
                </div>
                                
                           {{ form::close() }}
                            <!-- /a legend -->
                        
                    

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>

    <!-- Mirrored from demo.interface.club/limitless/layout_1/LTR/default/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 18 May 2018 11:14:21 GMT -->
</html>



