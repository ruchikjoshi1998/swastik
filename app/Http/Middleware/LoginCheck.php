<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class LoginCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Session::has('FacultyStatus'))   
        {
            return redirect('/admin_login');
        }
        return $next($request);
    }
}
