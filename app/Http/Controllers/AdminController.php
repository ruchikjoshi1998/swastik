<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Faculty;
use Validator;

class AdminController extends Controller
{
    public function login(Request $req){
    	$username =  $req->username;
    	$password =  $req->password;

    	$data = Faculty::where('email' , $username)->where('password' , md5($password))->first();
    	//$data = Faculty::all()->first();
    	if($data != '')
	      {
	        $req->Session()->put('username',$username);
	        $req->Session()->put('FacultyStatus',true);
	        return redirect('/dashboard');
	      }
	      else {
	          return redirect("/admin_login");
	      }
    }
    //Logout Admin
    public function logoutAdmin(Request $adminlogin)
    {

      $adminlogin->Session()->forget('username');
      $adminlogin->Session()->forget('FacultyStatus');
      return redirect("/admin_login");
    }

    //Add New Faculty
    public function addFaculty(Request $data)
    {
    	$users = new Faculty();

    	$validator=Validator::make($data->all(),[
            'name' => 'required|max:30|min:3',
            'image' => 'required',
            'phone' => 'required|digits:10',
            'email' => 'required|email|unique:faculty,email',
            'Password' => 'required|confirmed|min:6',
        ]);

	     if($validator->fails())
	     {
	     	
	       return redirect('/add_faculty')->withErrors($validator)->withInput();
	     }


     $users->name = $data->name;
     $users->phone = $data->phone;
     $users->email = $data->email;
     $users->joining_date = $data->date;

     //file move upload
     $file = $data->file('image');
     $destinationPath = 'img/user_image';
     $file->move($destinationPath,$file->getClientOriginalName());
     
     // Over file move upload
     $users->image = $file->getClientOriginalName();
     $users->password = md5($data->Password);
     

        $users->save();
     return redirect('/dashboard');
    }

    //Delete Faculty
    public function deleteFaculty(Request $req)
    {
    	$id = $req->id;
        $faculty = Faculty::find($id);
        $image_path =  "img/user_image/".$faculty['image'];
        echo $image_path;
      
        if(File::exists($image_path)) {
            File::delete($image_path);   
        }

    	Faculty::where('id', $id)->delete();
    	return back();
    }
    
}
