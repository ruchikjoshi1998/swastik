<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Faculty;

class LinkController extends Controller
{
    public function eventForm()
    {
    	return view('events');
    }
    public function galleryForm()
    {
    	return view('gallery');
    }
    public function adminLoginForm()
    {
    	return view('admin.login');
    }
    public function dashboard(Request $req)
    {
        $data = Controller::titleName($req);
        return view('admin.dashboard',['data'=>$data]);
    }
    public function addFaculty(Request $req)
    {
        $data = Controller::titleName($req);
        return view('admin.addFaculty',['data'=>$data]);
    }
    public function viewFaculty(Request $req)
    {
        $model = new Faculty();
        $faculty = $model::all();
        $data = Controller::titleName($req);
        return view('admin.viewFaculty',['data'=>$data],['faculty'=>$faculty]);
    }
}
